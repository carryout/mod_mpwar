class mod_mpwar{
class { 'apache':}
apache::vhost {'centos.dev':
	port	=> '80',
	docroot	=> '/www',
	docroot_owner => 'vagrant',
	docroot_group => 'vagrant',
    } 
apache::vhost {'project1.dev':
	port	=> '80',
	docroot	=> '/www/project1',
	docroot_owner => 'vagrant',
	docroot_group => 'vagrant',
    }
class { '::mysql::server':
    databases => {
      'mpwar_test' => {
        ensure  => 'present',
        charset => 'utf8',
      }
      }
}
#class { 'php': }


class{ 'iptables' :  }
iptables::rule { '8080': port => '8080', protocol => 'tcp',  }
iptables::rule { '80': port => '80', protocol => 'tcp',  }

host { 'localhost':
    ip => '127.0.0.1',
    host_aliases => ['mysql1','memcached1'],
}
#include epel

#class {'mod_preparephp':}


# PHP
$php_version = '55'

# remi_php55 requires the remi repo as well
if $php_version == '55' {
  $yum_repo = 'remi-php55'
  include ::yum::repo::remi_php55
}
# remi_php56 requires the remi repo as well
elsif $php_version == '56' {
  $yum_repo = 'remi-php56'
  ::yum::managed_yumrepo { 'remi-php56':
    descr          => 'Les RPM de remi pour Enterpise Linux $releasever - $basearch - PHP 5.6',
    mirrorlist     => 'http://rpms.famillecollet.com/enterprise/$releasever/php56/mirror',
    enabled        => 1,
    gpgcheck       => 1,
    gpgkey         => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-remi',
    gpgkey_source  => 'puppet:///modules/yum/rpm-gpg/RPM-GPG-KEY-remi',
    priority       => 1,
  }
}
# version 5.4
elsif $php_version == '54' {
  $yum_repo = 'remi'
  include ::yum::repo::remi
}

class { 'php':
  version => 'latest',
  require => Yumrepo[$yum_repo]
}

php::module { [ 'devel', 'pear', 'mbstring', 'xml', 'tidy', 'pecl-memcache', 'soap' ]: }



#class { 'memcached': }

#class { 'cowsaymsg':
#    msg => '¡Tengo frio!',
#    }
}